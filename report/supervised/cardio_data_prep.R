

setwd("C:/cardio_redux/supervised")

source("supervised_functions.R")

# Variables set by user ---------------------------------------------------

#And for cardiovirus
cardio.alignment <- readAAStringSet("cardio_seqs_aligned.FASTA")

#Identify where the EMCV cleavage sites are in the aligned sequence, by referring to the known TMEV sites
TMEV.3C.pos <- c(76, 414, 646, 922, 1191, 1517, 1605, 1625, 1842)
TMEV.name <- "lcl|DQ401688.1_prot_ABD67451.1_1[protein=polyprotein][protein_id=ABD67451.1][location=1067..7978]"
TMEV.pos <- which(names(cardio.alignment)==TMEV.name)
TMEV.seq <- cardio.alignment[TMEV.pos]
cardio.alignment <- as.character(cardio.alignment)
TMEV.seq <- c(as.character(TMEV.seq))

#Convert unaligned cleavage positions to aligned cleavage positions
cardio.cleavage.sites <- c()
for (i in 1:length(TMEV.3C.pos)){
  cardio.cleavage.sites <- c(cardio.cleavage.sites, unaligned.to.aligned.pos(cardio.alignment[TMEV.pos], TMEV.3C.pos[i])  )
}

cardio.cleavage.sites 


#Number of residues to include on the margin on each side of the cleavage side; note this will be an upper bound on the number of residues that can be included in models.
margin <- 10
#Set of amino acid codes in the order they will be used throughout
aa_codes <- c("G", "A", "L", "M", "F", "W", "K", "Q", "E", "S", "P", "V", "I", "C", "Y", "H", "R", "N", "D", "T")
#Read in amino acid properties for alternate encoding
aa_properties <- read.csv("Amino_acid_indices_clean.csv", header=TRUE)
colnames(aa_properties)[1] <- "Amino.acid"
aa_properties[, "Amino.acid"] <- as.character(aa_properties[, "Amino.acid"])
rownames(aa_properties) <- aa_properties[, "Amino.acid"]
aa_properties <- aa_properties[,2:ncol(aa_properties)]
colnames(aa_properties) <- c("en_transfer", "side_chain", "frac_occur", "ave_gain", "hydro_ind", "memb_pref", "ALTLS", "hydro_scale", "buried_ratio", "alpha_beta")

#Read in BLOSUM matrix for alternate encoding
blosum62 <- read.table("BLOSUM62.txt")
blosum62 <- blosum62[aa_codes, aa_codes]
score.matrix <- blosum62


# Set up NetSurf data -----------------------------------------------------

cardio.netsurf <- read.surf.data("cardio_netsurf.txt") 
cardio.names <- unique(cardio.netsurf$seq.name)
cardio.netsurf.aligned <- produce.aligned.netsurf(cardio.names, cardio.netsurf, cardio.alignment)


# Build data matrices -----------------------------------------------------

#Extract and tidy all positive sequences 
cardio_3C_cleave_seqs <- extract_seqs(cardio.alignment, cardio.cleavage.sites, margin)
#Remove X's
cardio_3C_cleave_seqs <- cardio_3C_cleave_seqs[!sapply(cardio_3C_cleave_seqs[,"sequence"], grepl, pattern="X"),]
#Convert cleavage sites to a binary matrix 
cardio_c_matrix <- formatted_bin_matrix(cardio_3C_cleave_seqs, margin, aa_codes, zero_or_one=1)

#Extract and tidy all negative sequences 
cardio_neg_seqs <- extract_seqs(cardio.alignment, seq(1:nchar(cardio.alignment[1])), margin, exclude_seqs=cardio_3C_cleave_seqs[,"sequence"])
#Remove X's
cardio_neg_seqs <- cardio_neg_seqs[!sapply(cardio_neg_seqs[,"sequence"], grepl, pattern="X"),]
#Convert to a binary matrix
cardio_nc_matrix <- formatted_bin_matrix(cardio_neg_seqs, margin, aa_codes, zero_or_one=0)

#Combine into single object for neural net analysis
cardio_all <- create.matrices(cardio_c_matrix, cardio_nc_matrix, aa_codes, aa_properties, blosum62)
cardio_data <- cardio_all[["bin_data"]]
cardio_prop_data <- cardio_all[["prop_data"]]
cardio_blos_data <- cardio_all[["blos_data"]]

cardio_inc.dups <- create.matrices(cardio_c_matrix, cardio_nc_matrix, aa_codes, aa_properties, blosum62, remove.dups=FALSE)
cardio_data_inc.dups <- cardio_inc.dups[["bin_data"]]
cardio_prop_data_inc.dups <- cardio_inc.dups[["prop_data"]]
cardio_blos_data_inc.dups <- cardio_inc.dups[["blos_data"]]

save(cardio_all, cardio_inc.dups, file="cardio_xxx.RData")

#Include relative and absolute exposure data
cardio_data <- add.surf.data(cardio.names, cardio.netsurf.aligned, cardio_data)
cardio_prop_data <- add.surf.data(cardio.names, cardio.netsurf.aligned, cardio_prop_data)
cardio_blos_data <- add.surf.data(cardio.names, cardio.netsurf.aligned, cardio_blos_data)

cardio_data_inc.dups <- add.surf.data(cardio.names, cardio.netsurf.aligned, cardio_data_inc.dups)
cardio_prop_data_inc.dups <- add.surf.data(cardio.names, cardio.netsurf.aligned, cardio_prop_data_inc.dups)
cardio_blos_data_inc.dups <- add.surf.data(cardio.names, cardio.netsurf.aligned, cardio_blos_data_inc.dups)

#Save prepared data
save(cardio_data, cardio_prop_data, cardio_blos_data, file="cardio_data_redux.RData")
save(cardio_data_inc.dups,  file="cardio_data_redux_inc_dups.RData")

nrow(cardio_data_inc.dups)








