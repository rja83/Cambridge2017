
source("unsupervised_functions.R")

# User-determined variables -----------------------------------------------

#Update to local folder
setwd("C:/Users/rja/Dropbox/rja83_Cambridge/unsupervised")

virus.name <- "cardio"
#Identify the parameters that were used in running MEME. These are used to set the file names below; alternatively, the user could just declare the file names directly.
motif.count <- 50
motif.length <- 4
meme.file.name <- paste0("MEME_", virus.name, "_enriched", "_m", motif.count, "_length", motif.length, ".txt")
fimo.enriched.file.name <- paste0("FIMO_", virus.name, "_enriched", "_m", motif.count, "_length", motif.length, ".txt")
fimo.testint.file.name <- paste0("FIMO_", virus.name, "_testint", "_m", motif.count, "_length", motif.length, ".txt")

# Produce analysis and plot ----------------------------------------------------------

#Read in MEME and FIMO (on enriched intervals) data and calculate alignment scores
MEME.data <- analyse.MEME.suite.results(meme.file.name, fimo.enriched.file.name, enriched.intervals)

#Include the analysis of FIMO scores from the test interval
MEME.data <- assess.test.interval(fimo.testint.file.name, MEME.data$MEME.data.summary)

#FInally, plot results
motif.plot(MEME.data, scale=TRUE, scale.var ="testint.sumlogpval", submotif.highlights=c("QG", "QS", "QE", "ES", "QN"), y.var = "align.score", ylabel="Alignment score", title="")



